import requests
import shutil
import urlparse, os
from time import sleep
import smtplib


class ClientException(Exception):
    pass


class Client(object):
    def __init__(self, base_url):
        self.server_url = "http://localhost:5000/"
        self.base_url = base_url
        self.output_folder = os.path.join(os.path.dirname(os.path.abspath(__file__)), "client_files")
        self.urls = []
        self.file_name_to_url = dict()

    def get_urls(self):
        r = self.retry_request(self.base_url, 'get')
        if r is None:
            raise ClientException("Error while trying to connect {}".format(self.base_url))
        self.urls = eval(r.text)

    def download_and_send_files(self):
        for url in self.urls:
            url_path = urlparse.urlparse(url)
            file_name = os.path.basename(url_path.path)
            file_contents = self.retry_request(url, 'get', stream=True)
            if file_contents is None:
                print "Error: while trying to download file from url : {}".format(url)
                continue
            full_output_filename = os.path.join(self.output_folder, file_name)
            self.file_name_to_url[file_name] = url
            if os.path.exists(full_output_filename):
                continue
            with open(full_output_filename, 'wb') as f:
                for chunk in file_contents.iter_content(chunk_size=1024):
                    if not chunk:
                        continue
                    f.write(chunk)

    def send_files_to_web_server(self):
        for filename in os.listdir(self.output_folder):
            full_output_filename = os.path.join(self.output_folder, filename)
            files = {'upload_file': open(full_output_filename, 'rb')}
            values = {'url': self.file_name_to_url[filename]}
            r = self.retry_request(self.server_url, 'post', files=files, data=values)
            if r is None:
                ClientException("Error while trying to send file : {} to server {}".format(full_output_filename,
                                                                                           self.server_url))
            #self.send_email(filename)

    @staticmethod
    def retry_request(url, method, retry=5, **kwargs):
        while retry:
            try:
                r = getattr(requests, method)(url, **kwargs)
                r.raise_for_status()
                return r
            except (requests.exceptions.SSLError, requests.HTTPError):
                sleep(0.5)
                retry -= 1
        return None

    def clear_folder(self):
        try:
            shutil.rmtree(self.output_folder)
        except WindowsError:
            pass
        os.mkdir(self.output_folder)

    @staticmethod
    def send_email(filename):
        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.login("blablatest", "blabla1")
        msg = "\n Send file {} to server was success!".format(filename)
        server.sendmail("test@gmail.com", "test2@gmail.com", msg)


if __name__ == '__main__':
    while True:
        c = Client('https://ic-ai.com/candidate/get_urls.php')
        c.clear_folder()
        c.get_urls()
        c.download_and_send_files()
        c.send_files_to_web_server()
        sleep(5 * 60)
