import os
import mysql.connector
from flask import Flask, request, jsonify, abort
from werkzeug.utils import secure_filename

app = Flask(__name__)

UPLOAD_FOLDER = os.path.join(os.path.dirname(os.path.abspath(__file__)), "server_files")
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


def insert_data_to_mysql(url, filename):
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="admin",
        database='server'
    )
    mycursor = mydb.cursor()
    sql = "INSERT INTO urls_to_files VALUES (%s, %s, %s)"
    val = (url, os.path.join(app.config['UPLOAD_FOLDER'], filename), filename)
    mycursor.execute(sql, val)
    mydb.commit()
    mydb.close()


def create_file_name(filename):
    i = 0
    base_file_name, ex = os.path.splitext(filename)
    while os.path.exists(os.path.join(app.config['UPLOAD_FOLDER'], '{}_{}'.format(base_file_name, i), ex)):
        i += 1
    return '{}_{}{}'.format(base_file_name, i, ex)


@app.route('/', methods=['POST', 'GET'])
def upload_file():
    if 'upload_file' not in request.files:
        return abort(400)

    f = request.files['upload_file']
    if f.filename == '':
        return abort(400)

    if f:
        filename = secure_filename(f.filename)
        filename = create_file_name(filename)
        f.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
    else:
        return abort(400)

    insert_data_to_mysql(request.form['url'], filename)
    return jsonify(success=True)


if __name__ == '__main__':
   app.run(debug=True)
